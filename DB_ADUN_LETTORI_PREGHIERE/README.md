# SCAI ORACLE BASELINE #

### In questo documento sono elencati gli step necessari per creare un db oracle 11g con docker e script SQL custom ###

* Preparazione ambiente
* Lancio immagine docker

### Preparazione ambiente ###

* Iniziare prima di tutto a clonare questo Repository
* Nella cartella script in cui abbiamo scaricato il repository inseriremo i file .sql per creare il nostro DB
* Vincoliamo la creazione di nuovi schema ad avere username e password identici
* Lanciando l'immagine docker con le istruzioni elencate sotto si avr� un db costruito secondo la struttura voluta

### Lancio immagine docker ###

* docker run -it --rm -v [path completo cartella template del repo locale]:/docker-entrypoint-initdb.d/script -e ACRONYM=[acronimo del db] --name [nome container] -p [porta locale]:22 -p [porta locale]:1521 wnameless/oracle-xe-11g
* esempio di script di start:
```
docker run -it -v "/G/Il mio Drive/DB_PROVA/script":/docker-entrypoint-initdb.d/script -e ACRONYM=ADUN --name db_adun0 -p 49165:22 -p 49163:1521 thecyber/repodaniel:db_adun0
```
* con l'esempio sopra ci si puo connettere al db locale con questi parametri
```
=> host: localhost
=> porta: 49163
=> SID: xe
=> user: ADUN_OWN / ADUN_APP
=> pass: ADUN_OWN / ADUN_APP
```


* qui potete trovare la [documentazione dell'imagine docker](https://hub.docker.com/r/wnameless/oracle-xe-11g/) usata per il db.