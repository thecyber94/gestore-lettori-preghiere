Installare ojdbc7.jar (scaricarlo online) ed installarlo alla repo locale maven con questo comando:
mvn install:install-file -Dfile=/Volumes/DATI/ojdbc7.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.1 -Dpackaging=jar

Avviare poi docker con:

docker run -it -v "/D/Personale/gestore-lettori-preghiere/DB_ADUN_LETTORI_PREGHIERE/script":/docker-entrypoint-initdb.d/script -e ACRONYM=ADUN --name db_adun0 -p 49165:22 -p 49163:1521 thecyber/repodaniel:db_adun0

MAC -->
docker run -it -v "/Volumes/SSHD\ 1\ Mac/Users/michelerusso/gestore-lettori-preghiere/DB_ADUN_LETTORI_PREGHIERE/script":/docker-entrypoint-initdb.d/script -e ACRONYM=ADUN --name db_adun0 -p 49165:22 -p 49163:1521 thecyber/repodaniel:db_adun0

