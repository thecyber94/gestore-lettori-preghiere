package com.scai.bo;

import java.util.Date;

public class GenericBO {
	
	private String nome;
	private String cognome;
	private Date dataLettura;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public Date getDataLettura() {
		return dataLettura;
	}
	public void setDataLettura(Date dataLettura) {
		this.dataLettura = dataLettura;
	}
	
	

}
