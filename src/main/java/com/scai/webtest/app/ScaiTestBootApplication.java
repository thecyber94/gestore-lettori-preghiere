package com.scai.webtest.app;

import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@ComponentScan({"com.scai*"})
@EntityScan("com.scai*")
@EnableJpaRepositories("com.scai*")
public class ScaiTestBootApplication extends SpringBootServletInitializer {
	
//	@Autowired
//    DataSource dataSource;
//
//	@Autowired
//	private AdunLettoriRepository adunLettoriRepository;

	public static void main(String[] args) {
		SpringApplication.run(ScaiTestBootApplication.class, args);
	}


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ScaiTestBootApplication.class);
	}

	@Bean
	public FacesServlet facesServlet() {
		return new FacesServlet();
	}

	@Bean
	public ServletRegistrationBean facesServletRegistration() {
		ServletRegistrationBean registration = new   ServletRegistrationBean(facesServlet(), "*.xhtml");
		return registration;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	//	@Bean
	//    DataSource dataSource() throws SQLException {
	//        OracleDataSource dataSource = new OracleDataSource();
	//        dataSource.setUser(username);
	//        dataSource.setPassword(password);
	//        dataSource.setURL(url);
	//        dataSource.setImplicitCachingEnabled(true);
	//        dataSource.setFastConnectionFailoverEnabled(true);
	//        return dataSource;
	//    }

	//	@Bean
	//	public ServletRegistrationBean pushServlet() {
	//		ServletRegistrationBean pushServlet = new ServletRegistrationBean(new PushServlet(), "/primepush/*");
	//		pushServlet.addInitParameter("org.atmosphere.annotation.packages", "org.primefaces.push");
	//		pushServlet.addInitParameter("org.atmosphere.cpr.broadcasterCacheClass", "org.atmosphere.cache.UUIDBroadcasterCache");
	//		pushServlet.addInitParameter("org.atmosphere.cpr.packages", "WEB-INF/classes/com.scai.webtest.app.notify,com.scai.webtest.app.notify");
	//		pushServlet.setAsyncSupported(true);
	//		pushServlet.setLoadOnStartup(1);
	//		pushServlet.setOrder(Ordered.HIGHEST_PRECEDENCE);
	//		return pushServlet;
	//	}


	//	private static class EmbeddedAtmosphereInitializer extends ContainerInitializer implements ServletContextInitializer {
	//
	//		@Override
	//		public void onStartup(ServletContext servletContext) throws ServletException {
	//			onStartup(Collections.<Class<?>> emptySet(), servletContext);
	//		}
	//
	//	}

	//	@Bean
	//	public EmbeddedAtmosphereInitializer atmosphereInitializer() {
	//		return new EmbeddedAtmosphereInitializer();
	//	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {

		servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());   
		servletContext.setInitParameter("primefaces.THEME", "bootstrap");                                   
		servletContext.setInitParameter("primefaces.CLIENT_SIDE_VALIDATION", Boolean.TRUE.toString());      
		servletContext.setInitParameter("javax.faces.FACELETS_SKIP_COMMENTS", Boolean.TRUE.toString());     
		servletContext.setInitParameter("primefaces.FONT_AWESOME", Boolean.TRUE.toString());                
		servletContext.setInitParameter("primefaces.UPLOADER", "commons");                                  

//		System.out.println("DATASOURCE = " + dataSource);
//
//		for(AdunLettori lettore : adunLettoriRepository.findAll()) {
//			System.out.println("Lettore => " + lettore.getId().getCognome());
//		}
		
		super.onStartup(servletContext);
	}
	
	@PostConstruct
	void started() {
	    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}
	

}
