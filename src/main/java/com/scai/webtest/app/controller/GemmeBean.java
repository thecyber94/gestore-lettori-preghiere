package com.scai.webtest.app.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.jsf.FacesContextUtils;

import com.scai.bo.GenericBO;
import com.scai.service.AdunGemmeService;


@ManagedBean
@ViewScoped
public class GemmeBean  extends SpringBeanAutowiringSupport{


	private static final Logger log = LoggerFactory.getLogger(GemmeBean.class);

	@Autowired
	private AdunGemmeService adunGemmeService;

	private List<GenericBO> gemmeList;
	private List<GenericBO> filteredGemme;

	private GenericBO gemma = new GenericBO();

	private boolean dataNulla;


	@PostConstruct
	public void init(){
		FacesContextUtils
		.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
		.getAutowireCapableBeanFactory().autowireBean(this);
		//log.debug("INIT HomeControllerBean ...");
		caricoGemme();

	}

	private void caricoGemme() {
		gemmeList = adunGemmeService.getAllGemme();
	}

	public void modificaGemma() {
		if(dataNulla) {
			gemma.setDataLettura(null);
		}
		adunGemmeService.updateGemma(gemma);
		caricoGemme();
		addMessage("Gemma modificata con successo");
		resettoCampi();
	}
	
	public void resettoCampi() {
		gemma = new GenericBO();
		dataNulla = false;
	}
	
	public void addGemma() {
		if(gemma.getCognome()==null || gemma.getCognome().equals("") || gemma.getNome()==null || gemma.getNome().equals("")) {
			addErrorMessage("Campo Nome / Cognome obbligatorio");
		} else {
			adunGemmeService.addGemma(gemma);
			caricoGemme();
			addMessage("Lettore inserito con successo");
			resettoCampi();
		}
	}

	public void removeGemma() {
		adunGemmeService.removeGemma(gemma);
		caricoGemme();
		addMessage("Gemma eliminata con successo --> ".concat(gemma.getCognome()));
	}


	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addErrorMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}



	public List<GenericBO> getGemmeList() {
		return gemmeList;
	}

	public void setGemmeList(List<GenericBO> gemmeList) {
		this.gemmeList = gemmeList;
	}


	public List<GenericBO> getFilteredGemme() {
		return filteredGemme;
	}

	public void setFilteredGemme(List<GenericBO> filteredGemme) {
		this.filteredGemme = filteredGemme;
	}

	public AdunGemmeService getAdunGemmeService() {
		return adunGemmeService;
	}

	public void setAdunGemmeService(AdunGemmeService adunGemmeService) {
		this.adunGemmeService = adunGemmeService;
	}

	public GenericBO getGemma() {
		return gemma;
	}

	public void setGemma(GenericBO gemma) {
		this.gemma = gemma;
	}

	public boolean isDataNulla() {
		return dataNulla;
	}

	public void setDataNulla(boolean dataNulla) {
		this.dataNulla = dataNulla;
	}












}
