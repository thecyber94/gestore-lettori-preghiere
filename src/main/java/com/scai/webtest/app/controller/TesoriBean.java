package com.scai.webtest.app.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.jsf.FacesContextUtils;

import com.scai.bo.GenericBO;
import com.scai.service.AdunTesoriService;

@ManagedBean
@ViewScoped
public class TesoriBean  extends SpringBeanAutowiringSupport{


	private static final Logger log = LoggerFactory.getLogger(TesoriBean.class);

	@Autowired
	private AdunTesoriService adunTesoriService;

	private List<GenericBO> tesoriList;
	private List<GenericBO> filteredTesori;

	private GenericBO tesoro = new GenericBO();

	private boolean dataNulla;


	@PostConstruct
	public void init(){
		FacesContextUtils
		.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
		.getAutowireCapableBeanFactory().autowireBean(this);
		//log.debug("INIT HomeControllerBean ...");
		caricoTesori();

	}

	private void caricoTesori() {
		tesoriList = adunTesoriService.getAllTesori();
	}

	public void modificaTesoro() {
		if(dataNulla) {
			tesoro.setDataLettura(null);
		}
		adunTesoriService.updateTesoro(tesoro);
		caricoTesori();
		addMessage("Oratore modificato con successo");
		resettoCampi();
	}
	
	public void resettoCampi() {
		tesoro = new GenericBO();
		dataNulla = false;
	}

	public void addTesoro() {
		if(tesoro.getCognome()==null || tesoro.getCognome().equals("") || tesoro.getNome()==null || tesoro.getNome().equals("")) {
			addErrorMessage("Campo Nome / Cognome obbligatorio");
		} else {
			adunTesoriService.addTesoro(tesoro);
			caricoTesori();
			addMessage("Lettore inserito con successo");
			resettoCampi();
		}
	}

	public void removeTesoro() {
		adunTesoriService.removeTesoro(tesoro);
		caricoTesori();
		addMessage("Oratore eliminato con successo --> ".concat(tesoro.getCognome()));
	}


	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addErrorMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public AdunTesoriService getAdunTesoriService() {
		return adunTesoriService;
	}

	public void setAdunTesoriService(AdunTesoriService adunTesoriService) {
		this.adunTesoriService = adunTesoriService;
	}

	public List<GenericBO> getTesoriList() {
		return tesoriList;
	}

	public void setTesoriList(List<GenericBO> tesoriList) {
		this.tesoriList = tesoriList;
	}

	public List<GenericBO> getFilteredTesori() {
		return filteredTesori;
	}

	public void setFilteredTesori(List<GenericBO> filteredTesori) {
		this.filteredTesori = filteredTesori;
	}

	public GenericBO getTesoro() {
		return tesoro;
	}

	public void setTesoro(GenericBO tesoro) {
		this.tesoro = tesoro;
	}

	public boolean isDataNulla() {
		return dataNulla;
	}

	public void setDataNulla(boolean dataNulla) {
		this.dataNulla = dataNulla;
	}













}
