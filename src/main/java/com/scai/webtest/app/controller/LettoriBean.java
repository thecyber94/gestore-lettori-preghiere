package com.scai.webtest.app.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.jsf.FacesContextUtils;

import com.scai.bo.GenericBO;
import com.scai.service.AdunLettoriService;

@ManagedBean
@ViewScoped
public class LettoriBean  extends SpringBeanAutowiringSupport{


	private static final Logger log = LoggerFactory.getLogger(LettoriBean.class);

	@Autowired
	private AdunLettoriService adunLettoriService;

	private List<GenericBO> lettoriList;
	private List<GenericBO> filteredLettori;

	private GenericBO lettore = new GenericBO();

	private boolean dataNulla;


	@PostConstruct
	public void init(){
		FacesContextUtils
		.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
		.getAutowireCapableBeanFactory().autowireBean(this);
		//log.debug("INIT HomeControllerBean ...");
		caricoLettori();

	}

	private void caricoLettori() {
		lettoriList = adunLettoriService.getAllLettori();
	}

	public void modificaLettore() {
		if(dataNulla) {
			lettore.setDataLettura(null);
		}
		adunLettoriService.updateLettore(lettore);
		caricoLettori();
		addMessage("Lettore modificato con successo");
		resettoCampi();
	}

	public void resettoCampi() {
		lettore = new GenericBO();
		dataNulla = false;
	}

	public void addLettore() {
		if(lettore.getCognome()==null || lettore.getCognome().equals("") || lettore.getNome()==null || lettore.getNome().equals("")) {
			addErrorMessage("Campo Nome / Cognome obbligatorio");
		} else {
			adunLettoriService.addLettore(lettore);
			resettoCampi();
			caricoLettori();
			addMessage("Lettore inserito con successo");
		}
	}

	public void removeLettore() {
		adunLettoriService.removeLettore(lettore);
		caricoLettori();
		addMessage("Lettore eliminato con successo --> ".concat(lettore.getCognome()));
	}


	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addErrorMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<GenericBO> getLettoriList() {
		return lettoriList;
	}


	public void setLettoriList(List<GenericBO> lettoriList) {
		this.lettoriList = lettoriList;
	}

	public GenericBO getLettore() {
		return lettore;
	}

	public void setLettore(GenericBO lettore) {
		this.lettore = lettore;
	}

	public List<GenericBO> getFilteredLettori() {
		return filteredLettori;
	}

	public void setFilteredLettori(List<GenericBO> filteredLettori) {
		this.filteredLettori = filteredLettori;
	}

	public AdunLettoriService getAdunLettoriService() {
		return adunLettoriService;
	}

	public void setAdunLettoriService(AdunLettoriService adunLettoriService) {
		this.adunLettoriService = adunLettoriService;
	}

	public boolean isDataNulla() {
		return dataNulla;
	}

	public void setDataNulla(boolean dataNulla) {
		this.dataNulla = dataNulla;
	}










}
