package com.scai.webtest.app.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.jsf.FacesContextUtils;

import com.scai.bo.GenericBO;
import com.scai.service.AdunPreghiereService;

@ManagedBean
@ViewScoped
public class PreghiereBean  extends SpringBeanAutowiringSupport{


	private static final Logger log = LoggerFactory.getLogger(PreghiereBean.class);

	@Autowired
	private AdunPreghiereService adunPreghiereService;

	private List<GenericBO> preghiereList;
	private List<GenericBO> filteredPreghiere;

	private GenericBO preghiera = new GenericBO();

	private boolean dataNulla;


	@PostConstruct
	public void init(){
		FacesContextUtils
		.getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
		.getAutowireCapableBeanFactory().autowireBean(this);
		//log.debug("INIT HomeControllerBean ...");
		caricoPreghiere();

	}

	private void caricoPreghiere() {
		preghiereList = adunPreghiereService.getAllPreghiere();
	}

	public void modificaPreghiera() {
		if(dataNulla) {
			preghiera.setDataLettura(null);
		}
		adunPreghiereService.updatePreghiera(preghiera);
		caricoPreghiere();
		addMessage("Lettore modificato con successo");
		resettoCampi();
	}
	
	public void resettoCampi() {
		preghiera = new GenericBO();
		dataNulla = false;
	}

	public void addPreghiera() {
		if(preghiera.getCognome()==null || preghiera.getCognome().equals("") || preghiera.getNome()==null || preghiera.getNome().equals("")) {
			addErrorMessage("Campo Nome / Cognome obbligatorio");
		} else {
			adunPreghiereService.addPreghiera(preghiera);
			caricoPreghiere();
			addMessage("Lettore inserito con successo");
			resettoCampi();
		}
	}

	public void removePreghiera() {
		adunPreghiereService.removePreghiera(preghiera);
		caricoPreghiere();
		addMessage("Lettore eliminato con successo --> ".concat(preghiera.getCognome()));
	}


	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public void addErrorMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public AdunPreghiereService getAdunPreghiereService() {
		return adunPreghiereService;
	}

	public void setAdunPreghiereService(AdunPreghiereService adunPreghiereService) {
		this.adunPreghiereService = adunPreghiereService;
	}

	public List<GenericBO> getPreghiereList() {
		return preghiereList;
	}

	public void setPreghiereList(List<GenericBO> preghiereList) {
		this.preghiereList = preghiereList;
	}

	public List<GenericBO> getFilteredPreghiere() {
		return filteredPreghiere;
	}

	public void setFilteredPreghiere(List<GenericBO> filteredPreghiere) {
		this.filteredPreghiere = filteredPreghiere;
	}

	public GenericBO getPreghiera() {
		return preghiera;
	}

	public void setPreghiera(GenericBO preghiera) {
		this.preghiera = preghiera;
	}

	public boolean isDataNulla() {
		return dataNulla;
	}

	public void setDataNulla(boolean dataNulla) {
		this.dataNulla = dataNulla;
	}












}
