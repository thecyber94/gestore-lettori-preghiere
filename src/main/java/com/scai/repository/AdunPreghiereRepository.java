package com.scai.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.scai.model.AdunPreghiere;


@Repository
public interface AdunPreghiereRepository extends CrudRepository<AdunPreghiere, Long> {
	@Query("from AdunPreghiere order by dataLettura")
    public Iterable<AdunPreghiere> findByDataLettura();
	
	@Transactional
	@Modifying
	@Query("update AdunPreghiere a set a.dataLettura = :dataLett where a.id.nome = :nom and a.id.cognome = :cognom")
    public void updatePreghiere(@Param("dataLett") Date dataLettura, @Param("nom") String nome, @Param("cognom") String cognome);
}
