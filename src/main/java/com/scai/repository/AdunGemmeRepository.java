package com.scai.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.scai.model.AdunGemme;


@Repository
public interface AdunGemmeRepository extends CrudRepository<AdunGemme, Long> {
	@Query("from AdunGemme order by dataLettura")
    public Iterable<AdunGemme> findByDataLettura();
	
	@Transactional
	@Modifying
	@Query("update AdunGemme a set a.dataLettura = :dataLett where a.id.nome = :nom and a.id.cognome = :cognom")
    public void updateGemma(@Param("dataLett") Date dataLettura, @Param("nom") String nome, @Param("cognom") String cognome);
}
