package com.scai.repository;

import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.scai.model.AdunLettori;


@Repository
public interface AdunLettoriRepository extends CrudRepository<AdunLettori, Long> {
	@Query("from AdunLettori order by dataLettura")
    public Iterable<AdunLettori> findByDataLettura();
	
	@Transactional
	@Modifying
	@Query("update AdunLettori a set a.dataLettura = :dataLett where a.id.nome = :nom and a.id.cognome = :cognom")
    public void updateLettore(@Param("dataLett") Date dataLettura, @Param("nom") String nome, @Param("cognom") String cognome);
}
