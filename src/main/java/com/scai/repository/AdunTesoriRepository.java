package com.scai.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.scai.model.AdunTesori;


@Repository
public interface AdunTesoriRepository extends CrudRepository<AdunTesori, Long> {
	@Query("from AdunTesori order by dataLettura")
    public Iterable<AdunTesori> findByDataLettura();
	
	@Transactional
	@Modifying
	@Query("update AdunTesori a set a.dataLettura = :dataLett where a.id.nome = :nom and a.id.cognome = :cognom")
    public void updateTesori(@Param("dataLett") Date dataLettura, @Param("nom") String nome, @Param("cognom") String cognome);
}
