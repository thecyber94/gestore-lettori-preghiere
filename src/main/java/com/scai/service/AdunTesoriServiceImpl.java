package com.scai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scai.bo.GenericBO;
import com.scai.model.AdunTesori;
import com.scai.model.AdunTesoriPK;
import com.scai.repository.AdunTesoriRepository;


@Service
public class AdunTesoriServiceImpl implements AdunTesoriService {
	
    @Autowired
    private AdunTesoriRepository adunTesoriRepository;
    
	public List<GenericBO> getAllTesori() {
		List<GenericBO> result = new ArrayList<GenericBO>();
		List<AdunTesori> tesoriList = (List<AdunTesori>) adunTesoriRepository.findByDataLettura();
		for (AdunTesori item : tesoriList) {
			GenericBO elem = new GenericBO();
			elem.setCognome(item.getId().getCognome());
			elem.setNome(item.getId().getNome());
			elem.setDataLettura(item.getDataLettura());
			result.add(elem);
		}
		return result;
	}

	public void addTesoro(GenericBO item) {
		AdunTesori tesoro = popoloEntityTesoro(item);
		adunTesoriRepository.save(tesoro);
	}
	
	public void updateTesoro(GenericBO item){
		adunTesoriRepository.updateTesori(item.getDataLettura(), item.getNome(), item.getCognome());
	}

	public void removeTesoro(GenericBO item) {
		AdunTesori elem = popoloEntityTesoro(item);
		adunTesoriRepository.delete(elem);
	}
	

	private AdunTesori popoloEntityTesoro(GenericBO item) {
		AdunTesori elem = new AdunTesori();
		elem.setDataLettura(item.getDataLettura());
		AdunTesoriPK itemNomeCogn = new AdunTesoriPK();
		itemNomeCogn.setCognome(item.getCognome());
		itemNomeCogn.setNome(item.getNome());
		elem.setId(itemNomeCogn);
		return elem;
	}

	
}
