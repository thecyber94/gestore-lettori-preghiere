package com.scai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scai.bo.GenericBO;
import com.scai.model.AdunPreghiere;
import com.scai.model.AdunPreghierePK;
import com.scai.repository.AdunPreghiereRepository;


@Service
public class AdunPreghiereServiceImpl implements AdunPreghiereService {
	
    @Autowired
    private AdunPreghiereRepository adunPreghiereRepository;
    
	public List<GenericBO> getAllPreghiere() {
		List<GenericBO> result = new ArrayList<GenericBO>();
		List<AdunPreghiere> preghiereList = (List<AdunPreghiere>) adunPreghiereRepository.findByDataLettura();
		for (AdunPreghiere item : preghiereList) {
			GenericBO elem = new GenericBO();
			elem.setCognome(item.getId().getCognome());
			elem.setNome(item.getId().getNome());
			elem.setDataLettura(item.getDataLettura());
			result.add(elem);
		}
		return result;
	}

	public void addPreghiera(GenericBO item) {
		AdunPreghiere lett = popoloEntityPreghiera(item);
		adunPreghiereRepository.save(lett);
	}
	
	public void updatePreghiera(GenericBO item){
		adunPreghiereRepository.updatePreghiere(item.getDataLettura(), item.getNome(), item.getCognome());
	}

	public void removePreghiera(GenericBO item) {
		AdunPreghiere elem = popoloEntityPreghiera(item);
		adunPreghiereRepository.delete(elem);
	}
	

	private AdunPreghiere popoloEntityPreghiera(GenericBO item) {
		AdunPreghiere elem = new AdunPreghiere();
		elem.setDataLettura(item.getDataLettura());
		AdunPreghierePK itemNomeCogn = new AdunPreghierePK();
		itemNomeCogn.setCognome(item.getCognome());
		itemNomeCogn.setNome(item.getNome());
		elem.setId(itemNomeCogn);
		return elem;
	}

	
}
