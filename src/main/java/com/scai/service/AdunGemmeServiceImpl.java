package com.scai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scai.bo.GenericBO;
import com.scai.model.AdunGemme;
import com.scai.model.AdunGemmePK;
import com.scai.repository.AdunGemmeRepository;


@Service
public class AdunGemmeServiceImpl implements AdunGemmeService {
	
    @Autowired
    private AdunGemmeRepository adunGemmeRepository;
    
	public List<GenericBO> getAllGemme() {
		List<GenericBO> result = new ArrayList<GenericBO>();
		List<AdunGemme> gemmeList = (List<AdunGemme>) adunGemmeRepository.findByDataLettura();
		for (AdunGemme gemm : gemmeList) {
			GenericBO gemma = new GenericBO();
			gemma.setCognome(gemm.getId().getCognome());
			gemma.setNome(gemm.getId().getNome());
			gemma.setDataLettura(gemm.getDataLettura());
			result.add(gemma);
		}
		return result;
	}

	public void addGemma(GenericBO lettore) {
		AdunGemme gemm = popoloEntityGemma(lettore);
		adunGemmeRepository.save(gemm);
	}
	
	public void updateGemma(GenericBO gemma){
		adunGemmeRepository.updateGemma(gemma.getDataLettura(), gemma.getNome(), gemma.getCognome());
	}

	public void removeGemma(GenericBO gemma) {
		AdunGemme gemm = popoloEntityGemma(gemma);
		adunGemmeRepository.delete(gemm);
	}
	

	private AdunGemme popoloEntityGemma(GenericBO gemma) {
		AdunGemme gemm = new AdunGemme();
		gemm.setDataLettura(gemma.getDataLettura());
		AdunGemmePK gemmNomeCogn = new AdunGemmePK();
		gemmNomeCogn.setCognome(gemma.getCognome());
		gemmNomeCogn.setNome(gemma.getNome());
		gemm.setId(gemmNomeCogn);
		return gemm;
	}

	
}
