package com.scai.service;

import java.util.List;

import com.scai.bo.GenericBO;

public interface AdunTesoriService {
    public List<GenericBO> getAllTesori();
    public void addTesoro(GenericBO lettore);
    public void removeTesoro(GenericBO lettore);
    public void updateTesoro(GenericBO lettore);
}
