package com.scai.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scai.bo.GenericBO;
import com.scai.model.AdunLettori;
import com.scai.model.AdunLettoriPK;
import com.scai.repository.AdunLettoriRepository;


@Service
public class AdunLettoriServiceImpl implements AdunLettoriService {
	
    @Autowired
    private AdunLettoriRepository adunLettoriRepository;
    
	public List<GenericBO> getAllLettori() {
		List<GenericBO> result = new ArrayList<GenericBO>();
		List<AdunLettori> lettoriList = (List<AdunLettori>) adunLettoriRepository.findByDataLettura();
		for (AdunLettori lett : lettoriList) {
			GenericBO lettore = new GenericBO();
			lettore.setCognome(lett.getId().getCognome());
			lettore.setNome(lett.getId().getNome());
			lettore.setDataLettura(lett.getDataLettura());
			result.add(lettore);
		}
		return result;
	}

	public void addLettore(GenericBO lettore) {
		AdunLettori lett = popoloEntityLettore(lettore);
		adunLettoriRepository.save(lett);
	}
	
	public void updateLettore(GenericBO lettore){
		adunLettoriRepository.updateLettore(lettore.getDataLettura(), lettore.getNome(), lettore.getCognome());
	}

	public void removeLettore(GenericBO lettore) {
		AdunLettori lett = popoloEntityLettore(lettore);
		adunLettoriRepository.delete(lett);
	}
	

	private AdunLettori popoloEntityLettore(GenericBO lettore) {
		AdunLettori lett = new AdunLettori();
		lett.setDataLettura(lettore.getDataLettura());
		AdunLettoriPK lettNomeCogn = new AdunLettoriPK();
		lettNomeCogn.setCognome(lettore.getCognome());
		lettNomeCogn.setNome(lettore.getNome());
		lett.setId(lettNomeCogn);
		return lett;
	}
	
}
