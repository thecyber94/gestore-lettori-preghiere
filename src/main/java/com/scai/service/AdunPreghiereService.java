package com.scai.service;

import java.util.List;

import com.scai.bo.GenericBO;

public interface AdunPreghiereService {
    public List<GenericBO> getAllPreghiere();
    public void addPreghiera(GenericBO lettore);
    public void removePreghiera(GenericBO lettore);
    public void updatePreghiera(GenericBO lettore);
}
