package com.scai.service;

import java.util.List;

import com.scai.bo.GenericBO;

public interface AdunGemmeService {
    public List<GenericBO> getAllGemme();
    public void addGemma(GenericBO gemma);
    public void removeGemma(GenericBO gemma);
    public void updateGemma(GenericBO gemma);
}
