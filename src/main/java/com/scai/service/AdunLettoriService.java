package com.scai.service;

import java.util.List;

import com.scai.bo.GenericBO;

public interface AdunLettoriService {
    public List<GenericBO> getAllLettori();
    public void addLettore(GenericBO lettore);
    public void removeLettore(GenericBO lettore);
    public void updateLettore(GenericBO lettore);
}
