package com.scai.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ADUN_LETTORI database table.
 * 
 */
@Entity
@Table(name="ADUN_LETTORI")
@NamedQuery(name="AdunLettori.findAll", query="SELECT a FROM AdunLettori a")
public class AdunLettori implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AdunLettoriPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_LETTURA")
	private Date dataLettura;

	public AdunLettori() {
	}

	public AdunLettoriPK getId() {
		return this.id;
	}

	public void setId(AdunLettoriPK id) {
		this.id = id;
	}

	public Date getDataLettura() {
		return this.dataLettura;
	}

	public void setDataLettura(Date dataLettura) {
		this.dataLettura = dataLettura;
	}

}