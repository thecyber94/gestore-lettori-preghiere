package com.scai.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ADUN_GEMME database table.
 * 
 */
@Entity
@Table(name="ADUN_GEMME")
@NamedQuery(name="AdunGemme.findAll", query="SELECT a FROM AdunGemme a")
public class AdunGemme implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AdunGemmePK id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_LETTURA")
	private Date dataLettura;

	public AdunGemme() {
	}

	public AdunGemmePK getId() {
		return this.id;
	}

	public void setId(AdunGemmePK id) {
		this.id = id;
	}

	public Date getDataLettura() {
		return this.dataLettura;
	}

	public void setDataLettura(Date dataLettura) {
		this.dataLettura = dataLettura;
	}

}