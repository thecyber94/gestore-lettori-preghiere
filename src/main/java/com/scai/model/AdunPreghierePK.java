package com.scai.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ADUN_PREGHIERE database table.
 * 
 */
@Embeddable
public class AdunPreghierePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String nome;

	private String cognome;

	public AdunPreghierePK() {
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AdunPreghierePK)) {
			return false;
		}
		AdunPreghierePK castOther = (AdunPreghierePK)other;
		return 
			this.nome.equals(castOther.nome)
			&& this.cognome.equals(castOther.cognome);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.nome.hashCode();
		hash = hash * prime + this.cognome.hashCode();
		
		return hash;
	}
}