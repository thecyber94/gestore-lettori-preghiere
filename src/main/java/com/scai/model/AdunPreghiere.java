package com.scai.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ADUN_PREGHIERE database table.
 * 
 */
@Entity
@Table(name="ADUN_PREGHIERE")
@NamedQuery(name="AdunPreghiere.findAll", query="SELECT a FROM AdunPreghiere a")
public class AdunPreghiere implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AdunPreghierePK id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_LETTURA")
	private Date dataLettura;

	public AdunPreghiere() {
	}

	public AdunPreghierePK getId() {
		return this.id;
	}

	public void setId(AdunPreghierePK id) {
		this.id = id;
	}

	public Date getDataLettura() {
		return this.dataLettura;
	}

	public void setDataLettura(Date dataLettura) {
		this.dataLettura = dataLettura;
	}

}