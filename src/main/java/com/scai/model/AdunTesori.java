package com.scai.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the ADUN_TESORI database table.
 * 
 */
@Entity
@Table(name="ADUN_TESORI")
@NamedQuery(name="AdunTesori.findAll", query="SELECT a FROM AdunTesori a")
public class AdunTesori implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private AdunTesoriPK id;

	@Temporal(TemporalType.DATE)
	@Column(name="DATA_LETTURA")
	private Date dataLettura;

	public AdunTesori() {
	}

	public AdunTesoriPK getId() {
		return this.id;
	}

	public void setId(AdunTesoriPK id) {
		this.id = id;
	}

	public Date getDataLettura() {
		return this.dataLettura;
	}

	public void setDataLettura(Date dataLettura) {
		this.dataLettura = dataLettura;
	}

}