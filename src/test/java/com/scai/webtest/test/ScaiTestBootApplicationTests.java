package com.scai.webtest.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import com.scai.webtest.app.ScaiTestBootApplication;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ScaiTestBootApplication.class)
@WebAppConfiguration
public class ScaiTestBootApplicationTests {

	@Test
	public void contextLoads() {
	}

}
